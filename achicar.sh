#!/bin/bash
ORIGEN="$1"
ARCHIVO=$(basename -- "$ORIGEN")
NOMBRE="${ARCHIVO%.*}"
EXTENSION="${ARCHIVO##*.}"
BORRAR="NO"
LIMITE="700"
ALTO=$(identify -format %h "${ORIGEN}")
ANCHO=$(identify -format %w "${ORIGEN}")
#TEMPORALP="$(mktemp /tmp/.XXX.${EXTENSION})"
TEMPORALP="$(mktemp /tmp/.XXX.jpg)"
echo "${ALTO}x${ANCHO}"
if [[ "${ALTO}" -gt "${LIMITE}" ]] || [[ "${ANCHO}" -gt "${LIMITE}" ]] ; then
  echo "convert -resize ${LIMITE}x${LIMITE} \"${ORIGEN}\" \"${TEMPORALP}\""
  if ( convert -resize ${LIMITE}x${LIMITE} "${ORIGEN}" "${TEMPORALP}" ) ; then
    rm -f "${ORIGEN}"
    mv -f "${TEMPORALP}" "${NOMBRE}.jpg"
  fi
else
  if [[ "${EXTENSION}" -ne "jpg" ]] ; then
    if ( convert "${ORIGEN}" "${TEMPORALP}" ) ; then
      rm -f "${ORIGEN}"
      mv -f "${TEMPORALP}" "${NOMBRE}.jpg"
    fi
  fi
fi
rm -f "${TEMPORALP}"
