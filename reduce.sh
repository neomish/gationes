#!/bin/bash

function uso() {
  #Mensaje de como se usa
  echo ""
  echo "USO: $0 archivo.png [-d]"
  echo " ej: $0 \"kinkong.png\""
  echo ""
  echo "RESULTADO \"kinkong.webp\""
  echo ""
  echo "OPCIONES"
  echo " -d: borra el archivo original"
  echo ""
}

# Revisar cantidad de parámetros rescibidos
if [ $# == 2 ] || [ $# == 1 ] ; then
  COMANDO="/opt/realesrgan/realesrgan-ncnn-vulkan"
  ORIGEN="$1"
  ARCHIVO=$(basename -- "$ORIGEN")
  EXTENSION="${ARCHIVO##*.}"
  BORRAR='NO'
  if [ $# == 2 ] ; then
    if [ "$2" == "-d" ] ; then
      BORRAR='SI'
    fi
  fi
  if [ "${EXTENSION}" == "webp" ]; then
    NOMBRE="${ARCHIVO%.*}-m"
  else
    NOMBRE="${ARCHIVO%.*}"
  fi
  ALTO=$(identify -format %h "${ORIGEN}")
  ANCHO=$(identify -format %w "${ORIGEN}")
  if [[ "${ALTO}" > "1200" ]] || [[ "${ANCHO}" > "1200" ]] ; then
    TEMPORALP="$(mktemp /tmp/.XXX.${EXTENSION})"
    convert -resize 1200x1200 "${ORIGEN}" "${TEMPORALP}"
    PASO="${ORIGEN}"
    ORIGEN="${TEMPORALP}"
  fi
  TEMPORAL="$(mktemp /tmp/.XXX.png)"
  if ( "${COMANDO}" -i "${ORIGEN}" -o "${TEMPORAL}" 2>&1 | grep "%" ) ; then
    if ( cwebp -quiet -q 75 "${TEMPORAL}" -o "${NOMBRE}.webp" 2>&1 > /dev/null ) ; then
      echo "100.00%"
      if [ "${BORRAR}" == "SI" ] ; then
        rm -f "${ORIGEN}" > /dev/null
        rm -f "${PASO}"   > /dev/null
        echo "$ARCHIVO borrado"
      fi
    else
      echo "Error x_X"
    fi
  else
    echo "Error X_x"
  fi
  rm -f "${TEMPORAL}" > /dev/null
  rm -f "${TEMPORALP}" > /dev/null
else
  uso
fi
