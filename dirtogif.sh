#!/bin/bash

function uso() {
  #Mensaje de como se usa
  echo ""
  echo "USO: $0 <carpeta>"
  echo ""
  echo " ej: $0 \"mi carpeta\""
  echo ""
}

# Revisar cantidad de parámetros rescibidos
if  [ $# -ne 1 ]; then
  uso
else
  CARPETA="$1"
  ffmpeg -r 30 -i "${CARPETA}"/%04d.png -r 30 ".${CARPETA}.mp4"
  ANCHO=$( mediainfo --Inform="Video;%Width%" ".${CARPETA}.mp4" )
  ffmpeg -i ".${CARPETA}.mp4" -vf "fps=30,scale=${ANCHO}:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 "${CARPETA}.gif"
  rm -f ".${CARPETA}.mp4" 
fi
