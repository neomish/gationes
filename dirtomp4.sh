#!/bin/bash

function uso() {
  #Mensaje de como se usa
  echo ""
  echo "USO: $0 <carpeta>"
  echo ""
  echo " ej: $0 \"mi carpeta\""
  echo ""
}

# Revisar cantidad de parámetros rescibidos
if  [ $# -ne 1 ]; then
  uso
else
  CARPETA="$1"
  ffmpeg -r 30 -i "${CARPETA}"/%04d.png -r 30 "${CARPETA}.mp4"
fi
