#!/bin/bash

function uso() {
  #Mensaje de como se usa
  echo ""
  echo "USO: $0 <archivo.mp4>"
  echo ""
  echo " ej: $0 \"corto.mp4\""
  echo ""
}

# Revisar cantidad de parámetros rescibidos
if  [ $# -ne 1 ]; then
  uso
else
  VIDEO="$1"
  ANCHO=$( mediainfo --Inform="Video;%Width%" "${VIDEO}" )
  ffmpeg -i "${VIDEO}" -vf "fps=30,scale=${ANCHO}:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 "${VIDEO}.gif"
fi
