#!/bin/bash 
# background color using ANSI escape

bgBk=$(tput setab 0) # black
bgRd=$(tput setab 1) # red
bgGn=$(tput setab 2) # green
bgYw=$(tput setab 3) # yellow
bgBe=$(tput setab 4) # blue
bgMa=$(tput setab 5) # magenta
bgCn=$(tput setab 6) # cyan
bgWe=$(tput setab 7) # white

# foreground color using ANSI escape

fgBk=$(tput setaf 0) # black
fgRd=$(tput setaf 1) # red
fgGn=$(tput setaf 2) # green
fgYw=$(tput setaf 3) # yellow
fgBe=$(tput setaf 4) # blue
fgMa=$(tput setaf 5) # magenta
fgCn=$(tput setaf 6) # cyan
fgWe=$(tput setaf 7) # white

# text editing options

txBd=$(tput bold) # bold
txHf=$(tput dim)  # half-bright
txUl=$(tput smul) # underline
txUe=$(tput rmul) # exit underline
txRv=$(tput rev)  # reverse
txSo=$(tput smso) # standout
txSe=$(tput rmso) # exit standout
txRs=$(tput sgr0) # reset attributes
