#!/bin/bash

function uso() {
  #Mensaje de como se usa
  echo ""
  echo "USO: $0 imagen.png [-d]"
  echo " ej: $0 \"kaley.png\""
  echo ""
  echo "RESULTADO \"kaley.webp\""
  echo ""
  echo "OPCIONES"
  echo " -d: borra el archivo original"
  echo ""
}

# Revisar cantidad de parámetros rescibidos
if [ $# == 2 ] || [ $# == 1 ] ; then
  COMANDO="/opt/realesrgan/realesrgan-ncnn-vulkan"
  ORIGEN="$1"
  ARCHIVO=$(basename -- "$ORIGEN")
  EXTENSION="${ARCHIVO##*.}"
  TEMPORAL="$(mktemp /tmp/.XXX.jpg)"
  BORRAR='NO'
  if [ $# == 2 ] ; then
    if [ "$2" == "-d" ] ; then
      BORRAR='SI'
    fi
  fi
  if [ "${EXTENSION}" == "webp" ]; then
    NOMBRE="${ARCHIVO%.*}-m"
  else
    NOMBRE="${ARCHIVO%.*}"
  fi
  #echo "ARCHIVO   : ${ARCHIVO}"
  #echo "NOMBRE    : ${NOMBRE}"
  #echo "EXTENSION : ${EXTENSION}"
  DESTINO="$2"
  #if ( "${COMANDO}" -i "${ORIGEN}" -o "${DESTINO}" 2>&1 | grep "%" ) ; then
  if ( "${COMANDO}" -i "${ORIGEN}" -o "${TEMPORAL}" 2>&1 | grep "%" ) ; then
    if ( cwebp -quiet -q 75 "${TEMPORAL}" -o "${NOMBRE}.webp" 2>&1 > /dev/null ) ; then
      echo "100.00%"
      if [ "${BORRAR}" == "SI" ] ; then
        rm -vf "${ORIGEN}"
      fi
    else
      echo "Error x_X"
    fi
  else
    echo "Error X_x"
  fi
  rm -f "${TEMPORAL}"
else
  uso
fi
