#!/bin/bash

function uso() {
  #Mensaje de como se usa
  echo ""
  echo "USO: $0 cara objetivo resultado"
  echo " ej: $0 \"yo.png\" \"superman.png\" \"yo-como-superman.png\""
  echo ""
}

# Revisar cantidad de parámetros rescibidos
if  [ $# -ne 3 ]; then
  uso
else
  COMANDO="/opt/roopb9/roop/run.py"
  CARA="$1"
  OBJETIVO="$2"
  RESULTADO="$3"
  # ----
  # Parametros que pueden cambiarse
  #usage: run.py [-h] [-s SOURCE_PATH] [-t TARGET_PATH] [-o OUTPUT_PATH] [--frame-processor FRAME_PROCESSOR [FRAME_PROCESSOR ...]] [--keep-fps] [--keep-frames]
  #              [--skip-audio] [--many-faces] [--reference-face-position REFERENCE_FACE_POSITION] [--reference-frame-number REFERENCE_FRAME_NUMBER]
  #              [--similar-face-distance SIMILAR_FACE_DISTANCE] [--temp-frame-format {jpg,png}] [--temp-frame-quality [0-100]]
  #              [--output-video-encoder {libx264,libx265,libvpx-vp9,h264_nvenc,hevc_nvenc}] [--output-video-quality [0-100]] [--max-memory MAX_MEMORY]
  #              [--execution-provider {tensorrt,cuda,cpu} [{tensorrt,cuda,cpu} ...]] [--execution-threads EXECUTION_THREADS] [-v]
  #
  #options:
  #  -h, --help                                                                 show this help message and exit
  #  -s SOURCE_PATH, --source SOURCE_PATH                                       select an source image
  #  -t TARGET_PATH, --target TARGET_PATH                                       select an target image or video
  #  -o OUTPUT_PATH, --output OUTPUT_PATH                                       select output file or directory
  #  --frame-processor FRAME_PROCESSOR [FRAME_PROCESSOR ...]                    frame processors (choices: face_swapper, face_enhancer, ...)
  #  --keep-fps                                                                 keep target fps
  #  --keep-frames                                                              keep temporary frames
  #  --skip-audio                                                               skip target audio
  #  --many-faces                                                               process every face
  #  --reference-face-position REFERENCE_FACE_POSITION                          position of the reference face
  #  --reference-frame-number REFERENCE_FRAME_NUMBER                            number of the reference frame
  #  --similar-face-distance SIMILAR_FACE_DISTANCE                              face distance used for recognition
  #  --temp-frame-format {jpg,png}                                              image format used for frame extraction
  #  --temp-frame-quality [0-100]                                               image quality used for frame extraction
  #  --output-video-encoder {libx264,libx265,libvpx-vp9,h264_nvenc,hevc_nvenc}  encoder used for the output video
  #  --output-video-quality [0-100]                                             quality used for the output video
  #  --max-memory MAX_MEMORY                                                    maximum amount of RAM in GB
  #  --execution-provider {tensorrt,cuda,cpu} [{tensorrt,cuda,cpu} ...]         available execution provider (choices: cpu, ...)
  #  --execution-threads EXECUTION_THREADS                                      number of execution threads
  #  -v, --version                                                              show program's version number and exit
  # ----
  #  Definiciones varias
  PARAMETROS="--keep-frames --skip-audio --many-faces --execution-provider cpu"
  source /opt/roopb9/roop/venv/bin/activate
  if ( "${COMANDO}" ${PARAMETROS} -s "${CARA}" -t "${OBJETIVO}" -o "${RESULTADO}" 2>&1 )  ; then
    echo "¡Listo! -_-"
  else
    echo "¡Pocta! X_x"
  fi
fi
